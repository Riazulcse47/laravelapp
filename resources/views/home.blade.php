<!DOCTYPE html>
<html lang="en">
<head>
  <title>Film web app</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>
  
<div class="container mt-3">
  <h1 class="text-center">Film Web Application</h1><br><br><br>
  <div>
         <button type="submit" class="btn btn-primary btn-lg pull-right">Add New Film</button>
     </div><br>
  <section id="widget-grid" >
  <div class="row">
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="jarviswidget jarviswidget-color-darken" id="wid-id-16" data-widget-sortable="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" role="widget">
              <header role="heading"> <span class="widget-icon"> <i class="fa fa-lock"></i> </span>
                
              </header>
              <div role="content">
                  <div class="jarviswidget-editbox"> </div>
                  <div class="widget-body">									
                      <div class="table-responsive">
                          <table class="table table-bordered stickyHeader ">
                              <thead>
                                  <tr>
                                      <th>Action</th>
                                      <th> Name</th>
                                      <th>Description</th>											
                                      <th>Release Date</th>							
                                      <th >Rating</th>
                                      <th >Ticket</th>
                                      <th >Price</th>
                                      <th >Country</th>
                                      <th>Genre</th>
                                      <th >Photo</th>
                                     
                                  </tr>
                              </thead>
                              
                              @foreach($films as $film)
                              <tr>
                              <td>     <a class="btn btn-info" href="{{ route('films.show',$film->id) }}">Show Details</a></td>
                              <td>{{$film->name}}</td>
                              <td>{{$film->description}}</td>
                              <td>{{$film->country}}</td>
                              <td>{{$film->price}}</td>
                              <td>{{$film->ticket}}</td>
                              <td>{{$film->genre}}</td>
                              
                              <td><img class="img-responsive" alt="" src="/images/{{ $film->image }}" /></td>
                              <!-- <tbody>
                              
                               

                                      </tbody> -->
                                   </tr>
                                   @endforeach
                                  </table>
                              </div>
                              
                        
                      </div>
                  </div>
              </div>
          </article>
          <!-- WIDGET END -->
      </div>
      <!-- row -->
      </section>

</div>
</div>

</body>
</html>

