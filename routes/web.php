<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/', ['as' => 'home', 'uses' => 'ContentController@index']);
Route::resource('comment', 'CommentController', ['only' => ['create','store']]);

// Authentication
Route::get('admin/login', array('as' => 'admin.login', 'uses' => 'Auth\AuthController@getLogin'));
Route::post('admin/login', array('as' => 'admin.login', 'uses' => 'Auth\AuthController@postLogin'));
Route::get('admin/logout', array('as' => 'admin.logout', 'uses' => 'Auth\AuthController@getLogout'));

// Backend Admin with Authentication
Route::group(array('prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'), function()
{
    Route::post('content/sortlist', ['as' => 'admin.content.sortlist', 'uses' => 'ContentController@sortList']);
    Route::resource('content', 'ContentController', ['except' => ['show']]);
    Route::resource('comment', 'CommentController', ['only' => ['index','destroy']]);
});