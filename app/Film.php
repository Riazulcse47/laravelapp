<?php
namespace App;


use Illuminate\Database\Eloquent\Model;


class Film extends Model
{


    public $fillable = ['name','description','release_date','ticket','country','price','genre','photo'];


}