<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Film;


class filmController extends Controller
{


    public function manageVue()
    {
        return view('home');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Item::latest()->paginate(3);


        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];


        return response()->json($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'country' => 'required',
            'price' => 'required',
            'ticket' => 'required',
            'genre' => 'required',
            'photo' => 'required',
        ]);


        $create = Item::create($request->all());


        return response()->json($create);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'country' => 'required',
            'price' => 'required',
            'ticket' => 'required',
            'genre' => 'required',
            'photo' => 'required',
        ]);


        $edit = Item::find($id)->update($request->all());


        return response()->json($edit);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::find($id)->delete();
        return response()->json(['done']);
    }

    public function commentcheck(Request $request)
 {
    if (!Auth::check()) { 
        $request->session()->flash('login', 'You must be logged in to post a comment');
        return redirect()->back();
    }

    $user = Auth::user();
    $data = [
        'post_id' => $request->post_id,
        'author' => $user->name,
        'email' => $user->email,
        'body' => $request->body
    ];

    if (Comment::create($data)) {
        $request->session()->flash('comment_success', 'Your comment have been submitted and is waiting moderation');
        return redirect()->back(); 
    } 

    $request->session()->flash('comment_error', 'Your comment does not submited. Try again!!');
    return redirect()->back();
}
}
