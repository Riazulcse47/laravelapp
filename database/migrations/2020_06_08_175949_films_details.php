<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FilmsDetails extends Migration
{
    public function up()
    {
        Schema::create('films_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('country');
            $table->int('price');
            $table->string('genre');
            $table->text('ticket');
            $table->string('photo');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop("films_details");
    }
}
